<?php

use App\Http\Controllers\Reports\ShortenerReportsController;
use App\Http\Controllers\ShortenerController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|

    POST /links { "link":"string", "expire_date": "datetime", "custom_link": "string" }

    Get  /reports/links/{shortLink}/views -> {"views": int}
    Get  /reports/links/{shortLink}/histogram-views -> [{"start_date":"datetime","end_date":"datetime",views": int}]
*/



Route::group(['prefix' => 'links'], function () {
    Route::post('/', [ShortenerController::class, 'store'])->name('store_link');
});
Route::group(['namespace' => 'Reports', 'prefix' => 'reports/links'], function () {
    Route::get('/{shortLink}/views', [ShortenerReportsController::class, 'index'])->name('get_views');
});
