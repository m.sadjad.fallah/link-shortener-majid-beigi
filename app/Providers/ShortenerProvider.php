<?php

namespace App\Providers;

use App\Components\Shortener\DefaultShortenerStrategy;
use App\Components\Shortener\ShortenerGeneratorHandler;
use Illuminate\Support\ServiceProvider;

class ShortenerProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind(ShortenerGeneratorHandler::class, function() {
            return new ShortenerGeneratorHandler(new DefaultShortenerStrategy());
        });
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
    }
}
