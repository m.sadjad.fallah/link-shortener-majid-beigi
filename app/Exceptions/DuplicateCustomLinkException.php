<?php

namespace App\Exceptions;


use Exception;
use Illuminate\Http\JsonResponse;

class DuplicateCustomLinkException extends Exception
{
    protected $message = 'Duplicate custom link';

    /**
     * Render the exception into an HTTP response.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function render($request)
    {
        return new JsonResponse($this->getMessage(),409);
    }
}