<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Links extends Model
{
    use HasFactory;

    protected $fillable = ['link', 'custom_link','short_link', 'expire_date'];

    protected $dates = ['expire_date'];
}
