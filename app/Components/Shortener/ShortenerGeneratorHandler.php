<?php


namespace App\Components\Shortener;


use App\Exceptions\DuplicateCustomLinkException;
use App\Models\Links;
use Carbon\Carbon;

final class ShortenerGeneratorHandler
{
    /**
     * @var ShortenerStrategyInterface
     */
    private $shortLinkStrategy;

    public function __construct(ShortenerStrategyInterface $shortLinkStrategy)
    {
        $this->shortLinkStrategy = $shortLinkStrategy;
    }

    /**
     * @param $link
     * @param null $customLink
     * @param null $expireDate
     * @return Links
     * @throws DuplicateCustomLinkException
     */
    public function handle($link, $customLink = null, $expireDate = null): Links
    {
        if(!empty($customLink) && Links::query()
                ->where([
            ['custom_link','=', $customLink]
        ])
//            ->whereRaw('select short_link from links where short_link != ? ',[$customLink])
            ->exists()) {
            throw new DuplicateCustomLinkException();
        }
        $expireDate = !empty($expireDate) ? Carbon::parse($expireDate) : now()->addHour();
        $shortLink = $this->shortLinkStrategy->generate();
        return Links::firstOrCreate(
            ['short_link' => $shortLink],
            ['link' => $link,'custom_link'=> $customLink, 'short_link' => $shortLink, 'expire_date' => $expireDate]
        );
    }
}