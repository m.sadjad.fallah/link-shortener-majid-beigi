<?php

namespace App\Components\Shortener;

use App\Models\Links;

class DefaultShortenerStrategy implements ShortenerStrategyInterface
{
    public function generate(): string
    {
        $length = 5;
        do {
            $short = $this->getRandomString($length);
            if (!Links::query()->where('short_link', '=', $short)->exists()) {
                return $short;
            }
            $length++;
        } while ($length < 255);
    }

    public function getRandomString($length = 5)
    {
        $chars = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ_-';
        return substr(str_shuffle($chars), 0, $length);
    }
}