<?php

namespace App\Components\Shortener;


interface ShortenerStrategyInterface
{
    public function generate() : string;
}