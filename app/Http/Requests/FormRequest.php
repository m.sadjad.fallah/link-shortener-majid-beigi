<?php


namespace App\Http\Requests;

use Illuminate\Contracts\Validation\Validator;
use Illuminate\Foundation\Http\FormRequest as FR;
use Illuminate\Http\Exceptions\HttpResponseException;

class FormRequest extends FR
{
    protected $statusCode = 422;

    protected function failedValidation(Validator $validator) {
        throw new HttpResponseException(response()->json($validator->errors(), $this->statusCode));
    }
}