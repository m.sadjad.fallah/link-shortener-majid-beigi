<?php

namespace App\Http\Requests;

use App\Rules\UrlRule;

class ShortenerStoreLinkRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'link' => ['required', 'string', new UrlRule()],
            'expire_date' => 'nullable|date'
        ];
    }
}
