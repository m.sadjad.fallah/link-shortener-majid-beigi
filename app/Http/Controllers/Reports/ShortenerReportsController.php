<?php

namespace App\Http\Controllers\Reports;


use App\Http\Controllers\Controller;
use Illuminate\Http\JsonResponse;
use Illuminate\Support\Facades\Cache;

class ShortenerReportsController extends Controller
{
    public function index($shortLink)
    {
        return new JsonResponse(['views' => Cache::get('views:' . $shortLink)]);
    }
}