<?php

namespace App\Http\Controllers;

use App\Components\Shortener\ShortenerGeneratorHandler;
use App\Exceptions\NotFoundLinkException;
use App\Http\Requests\ShortenerStoreLinkRequest;
use App\Http\Resources\ShortenerResource;
use App\Jobs\LogViewsJob;
use App\Models\Links;

class ShortenerController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @param ShortenerStoreLinkRequest $request
     * @param ShortenerGeneratorHandler $shortenerGeneratorHandler
     * @return ShortenerResource
     */
    public function store(
        ShortenerStoreLinkRequest $request,
        ShortenerGeneratorHandler $shortenerGeneratorHandler
    ): ShortenerResource {

        $link = $request->get('link');
        $expireDate = $request->get('expire_date');
        $customLink = $request->get('custom_link');
        $shortLink = $shortenerGeneratorHandler->handle($link, $customLink, $expireDate);
        return new ShortenerResource($shortLink);
    }

    public function getLink($shortLink)
    {
        $shortLinkModel = Links::query()
            ->where('expire_date', '>=', now()->toDateTimeString())
            ->where(function ($query) use ($shortLink) {
                $query
                    ->orWhere('short_link', '=', $shortLink)
                    ->orWhere('custom_link', '=', $shortLink);
            })
            ->first();
        if (!$shortLinkModel) {
            throw new NotFoundLinkException();
        }
        dispatch(new LogViewsJob($shortLink));

        return redirect($shortLinkModel->link, 302);
    }
}
